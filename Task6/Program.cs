﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Task3
{
    class Program
    {
        //Initialize a list of people using different constructors
        private static List<Person> listOfNames = new List<Person> {
            new Person("Ola","Norman",123321123),
            new Person("Knut","Norman"),
            new Person("Ola","Sverre",444455555),
            new Person("Geir",939393939),
            new Person("Anne","Beate"),
            new Person("Ole"),
        };
        static void Main(string[] args)
        {
            //loop until user exits
            bool done = false;
            while (!done)
            {
                string searchTerm = PromptUserForInput();
                ShowMatchingNames(searchTerm);
                done=CheckIfUserIsDone();
            }
        }

        private static bool CheckIfUserIsDone()
        {
            //Function to check if user is done with the session
            Console.WriteLine();
            Console.WriteLine("Press q to quit, or any key to try again");
            return Console.ReadKey().Key == ConsoleKey.Q;
        }


        private static void ShowMatchingNames(string searchTerm)
        {
            //Function that will clear the screen, then print all names matching query
            Console.Clear();
            Console.WriteLine("Here are the matching names:");
            Console.WriteLine();
            Console.WriteLine(string.Format("{0,10} {1,10} {2,10}", "First name", "Last name", "Phonenumber"));
            Console.WriteLine("---------------------------------");

            foreach (Person searchMatch in listOfNames.FindAll(person => person.IsAtLeastPartialSearchMatch(searchTerm)))
            {
                Console.WriteLine(searchMatch);
            }
        }

        private static string PromptUserForInput()
        {
            //Function that prompts the user for input, and returns the string they type
            Console.Clear();
            Console.WriteLine("Pleas enter a search term:");
            string searchTerm = Console.ReadLine();
            return searchTerm;
        }
    }
}
