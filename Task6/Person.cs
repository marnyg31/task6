﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task3
{
    class Person
    {
        //Defining properties
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PhoneNumber { get; set; }

        public Person(string firstName, string lastName, int phoneNumber)
        {
            //Defining constructor
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
        }

        public Person(string firstName, string lastName)
        {
            //Overloading constructor
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = 0;
        }

        public Person(string firstName)
        {
            //Overloading constructor
            FirstName = firstName;
            LastName = "";
            PhoneNumber = 0;
        }

        public Person(string firstName, int phoneNumber)
        {
            //Overloading constructor
            FirstName = firstName;
            LastName = "";
            PhoneNumber = phoneNumber;
        }

        public bool IsAtLeastPartialSearchMatch(string searchTerm)
        {
            //Function to check if search string is present in any of the objects properties 
            return FirstName.Contains(searchTerm) ||
            LastName.Contains(searchTerm) ||
            PhoneNumber.ToString().Contains(searchTerm);
        }

        public override string ToString()
        {
            //Override default ToString to print prettie
            return string.Format("{0,10} {1,10} {2,10}", FirstName, LastName, PhoneNumber);
        }
    }
}
